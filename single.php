<?php get_header(); ?>
<div class="container">
    <div class="row mt-4 brand-font">
        <div class="d-flex justify-content-center mb-4 img-repo">
            <?php dynamic_sidebar('adsnine'); ?>
        </div>

        <div class="col-md-9">
            <?php
            while (have_posts()) : the_post(); {
            ?>
                    <div class="post-title text-dark">
                        <h1 class="h1 fw-bold"><?php the_title(); ?></h1>
                    </div>
                    <div class="my-3">
                        <i class="far fa-clock "></i> <span class="brand-font"><?php the_date(); ?></span>
                    </div>
                    <div class="img">
                        <?php if (has_post_thumbnail()) { ?>
                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                        <?php  } ?>
                    </div>


                    <div class="single-page img-repo my-4 text-dark" style="font-size: 22px;">
                        <?php the_content(); ?>
                    </div>

                    <div class="">
                        <?php echo do_shortcode('[TheChamp-FB-Comments]') ?>
                    </div>

                    <div class="container">
                        <div class="d-flex justify-content-center mb-4 img-repo">
                            <?php dynamic_sidebar('adsten'); ?>
                        </div>
                    </div>

            <?php
                }

            endwhile;
            ?>
        </div>

        <div class="col-md-3">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>