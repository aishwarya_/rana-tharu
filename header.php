<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?></title>

    <?php wp_head(); ?>

</head>

<body>

<div class="container-fluid">
    <div class="row">
      <div class="col-md-3 mx-0">
        <div class="row d-flex justify-content-start">
          <div class="col-md-12 text-center brand-logo mt-3">
            <?php if (has_custom_logo()) :
              $custom_logo_id = get_theme_mod('custom_logo');
              $image = wp_get_attachment_image_src($custom_logo_id, 'large');
            ?>
              <a href="<?php echo esc_url(home_url()); ?>">
                <img class="logo" src="<?php echo $image['0']; ?>" alt="" style="max-height: 120px;">
              </a>
            <?php endif ?>
          </div>
        </div>
      </div>

      <div class="col-md-9 mx-0 mt-3 text-center">
        <div class="d-flex justify-content-center img-repo">
          <?php dynamic_sidebar('adsone'); ?>
        </div>
      </div>

    </div>
    <div class="p-2 mx-4 brand-font"><?php echo get_nepali_today_date(); ?> <span class="mx-3"><?php echo date(get_option('date_format')); ?></span></div>

  </div>

  <div class="container-fluid sticky-top" style="background-color: #3a7416;">
    <div class="row">
      <div class="col-md-12">
        <nav class=" container-fluid navbar-expand-lg brand-font p-2">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#primarymenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <i class="fas fa-bars text-white"></i>

          </button>
          <?php
          wp_nav_menu(array(
            'theme_location'  => 'primary',
            'depth'           => 2,
            'container'       => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id'    => 'primarymenu',
            'menu_class'      => 'navbar-nav  justify-content-between w-100 primary-menu',
            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
            'walker'          => new WP_Bootstrap_Navwalker(),
          ));
          ?>
        </nav>

      </div>
    </div>
  </div>
