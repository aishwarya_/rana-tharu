<?php get_header(); ?>

<?php get_template_part('partials/section', 'featured'); ?>

<?php get_template_part('partials/section', 'samachar'); ?>

<?php get_template_part('partials/section', 'bichar'); ?>

<?php get_template_part('partials/section', 'sports'); ?>

<?php get_template_part('partials/section', 'photo-feature'); ?>

<?php get_template_part('partials/section', 'video'); ?>

<?php get_footer(); ?>