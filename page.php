<?php get_header(); ?>

<div class="container">
    <div class="row mt-3 brand-font">
        <?php
        if (have_posts()) : the_post();
        ?>
            <div class="border-bottom text-danger mb-1">
                <h1><?php the_title(); ?></h1>

            </div>
            <div class="single-page img-repo my-4 text-dark" style="font-size: 22px;">
                <?php the_content(); ?>
            </div>

        <?php
        endif;
        ?>
    </div>
</div>

<?php get_footer(); ?>