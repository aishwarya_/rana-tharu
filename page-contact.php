<?php get_header(); ?>

<div class="container">
    <div class="row mt-3">
        <div class="heading brand-font">
            <h1 class=" text-center my-2 fw-bold text-primary"><?php the_title(); ?></h1>
        </div>

        <div class="line">
            <div class="row mb-2 p-3">
                <div class="col-md-4 bg-primary p-1"></div>
                <div class="col-md-4 bg-danger p-1"></div>
                <div class="col-md-4 bg-primary p-1"></div>
            </div>
        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-8 brand-font">

            <h6 class="h5"><?php the_content(); ?></h6>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>


<?php get_footer(); ?>