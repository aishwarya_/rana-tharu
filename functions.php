<?php

require_once get_template_directory() . '/includes/widgets/theme_widgets.php';

require_once get_template_directory() . '/includes/customizer/social_setting_customizer.php';

// registering navwalker
function register_navwalker()
{
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

// links
if (!function_exists('theme_enquee_scripts')) {
    function theme_enquee_scripts()
    {

        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

        wp_enqueue_style('mdb', get_template_directory_uri() . '/mdb/css/mdb.min.css');

        wp_enqueue_style('utils', get_template_directory_uri() . '/css/utils.css', array(), '1.0.1');

        wp_enqueue_style('fontawesome', get_template_directory_uri() . '/fontawesome/css/all.css');

        wp_enqueue_style('Slick_CSS', get_template_directory_uri() . '/slick/slick.css');
        wp_enqueue_style('Slick_Theme', get_template_directory_uri() . '/slick/slick-theme.css');
        wp_enqueue_style('Slick_theme', get_template_directory_uri() . '/css/slickmod.css');

        wp_enqueue_style('font_cdn', 'https://fonts.googleapis.com/css2?family=Noto+Sans:wght@400;700&display=swap');

        wp_enqueue_script('JQuery', get_template_directory_uri() . '/js/jquery-3.6.0.min.js');

        wp_enqueue_script('bootstrap_JS', get_template_directory_uri() . '/js/bootstrap.min.js');

        wp_enqueue_script('Slick_JS', get_template_directory_uri() . '/slick/slick.min.js');

        wp_enqueue_script('Script', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true);

        wp_enqueue_script('mdb_js', get_template_directory_uri() . '/mdb/js/mdb.min.js');
    }
}
add_action('wp_enqueue_scripts', 'theme_enquee_scripts');

// for logo
function themename_custom_logo_setup()
{
    $defaults = array(
        'height'      => 120,
        'width'       => 120,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'themename_custom_logo_setup');

// for menu

add_theme_support('menus');
function wp_theme_setup()
{
    register_nav_menus(array(
        'primary' => __('Primary Menu', 'primary menu'),

        // 'footer-menu' => __('Footer Menu', 'footer menu'),

        'mobile-menu' => __('Mobile Menu Location', 'Mobile Menu'),
    ));
}
add_action('after_setup_theme', 'wp_theme_setup');

// for static image 
add_theme_support('post-thumbnails');

// for readmore
function custom_excerpt()
{
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 500);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    $excerpt = $excerpt . '<a href="' . get_the_permalink() . '">       [...]</a>';
    return $excerpt;
}

// to replace Howdy to Welcome

add_filter('admin_bar_menu', 'replace_wordpress_howdy', 25);
function replace_wordpress_howdy($wp_admin_bar)
{
    $my_account = $wp_admin_bar->get_node('my-account');
    $newtext = str_replace('Howdy,', 'Welcome,', $my_account->title);
    $wp_admin_bar->add_node(array(
        'id' => 'my-account',
        'title' => $newtext,
    ));
}

// for readmore
function custom_news()
{
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 1100);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    $excerpt = $excerpt . '<a href="' . get_the_permalink() . '">       [...]</a>';
    return $excerpt;
}

// giving access to editot in widgets

function extend_editor_caps()
{

    $roleObject = get_role('editor');

    if (!$roleObject->has_cap('edit_theme_options')) {
        $roleObject->add_cap('edit_theme_options');
    }
}
add_action('admin_init', 'extend_editor_caps');
