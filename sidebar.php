<div class="container">

    <div class="heading brand-font" style="background-color: green;">
        <h1 class="h2 text-center my-2 fw-bold text-white">ताजा अपडेट</h1>
    </div>

    <div class="row">


        <!-- query -->
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => '8',
            'category__not_in' => [12, 14],

            // 'order' => 'ASC'
        );
        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="card text-center mb-3">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <?php if (has_post_thumbnail()) : ?>
                            <img width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                        <?php endif; ?>
                    </a>
                    <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                        <h1 class=" h5 text-dark brand-font my-3"><?php the_title(); ?></h1>
                    </a>
                </div>


        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>