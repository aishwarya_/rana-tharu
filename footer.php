<footer>
    <div class="container-fluid" style="background-color: #3a7416;">
        <div class="container">
            <div class="row border-bottom">
                <div class="col-md-4 brand-font text-white">
                    <?php if (is_active_sidebar('about-us')) : ?>
                        <div class="ml-auto align-self-center mt-3 mt-md-0 ">
                            <?php dynamic_sidebar('about-us'); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-md-4 text-white brand-font">
                    <?php if (is_active_sidebar('ourteam')) : ?>
                        <div class="ml-auto align-self-center mt-3 mt-md-0 ">
                            <?php dynamic_sidebar('ourteam'); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-md-4 text-white brand-font">
                    <?php if (is_active_sidebar('contact')) : ?>
                        <div class="ml-auto align-self-center mt-3 mt-md-0 ">
                            <?php dynamic_sidebar('contact'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row text-white mt-1">
                <div class="col-md-6 my-2">
                    <p class="h5">© Copyright 2021. All Rights Reserved.</p>
                </div>
                <div class="col-md-6 my-2">
                    <p class="h5 text-end">Developed by <a class="text-white" target="_blank" href="https://mohrain.com/">Mohrain Websoft (P) Ltd.</a> </p>
                </div>
            </div>
        </div>
    </div>

</footer>


<?php wp_footer(); ?>

</body>

</html>