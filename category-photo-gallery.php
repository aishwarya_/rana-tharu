<?php get_header(); ?>

<?php
$obj = get_queried_object();
?>
<div class="container">
    <div class="row mt-4">
        <div class="d-flex justify-content-center mb-4 img-repo">
            <?php dynamic_sidebar('adsthree'); ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="heading brand-font">
        <h1 class="h1 text-center my-2 fw-bold text-dark">
            <?php echo $obj->name ?>
        </h1>
    </div>

    <div class="line">
        <div class="row my-2 p-3">
            <div class="col-md-4 bg-primary p-1"></div>
            <div class="col-md-4 bg-danger p-1"></div>
            <div class="col-md-4 bg-primary p-1"></div>
        </div>
    </div>

</div>

<div class="container">
    <div class="row mt-2">
        <!-- query -->
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'post',
            'category_name' => 'photo-gallery',
            'posts_per_page' => 8,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();
        ?>

                <div class="col-md-3">
                    <a href="<?php the_permalink(); ?>">
                        <?php if (has_post_thumbnail()) : ?>
                            <img width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                        <?php endif; ?>
                    </a>
                    <div class="my-4">
                        <a href="<?php the_permalink(); ?>">
                            <h4 class=" h4 card-title text-center text-dark brand-font"><?php the_title(); ?></h4>
                        </a>


                    </div>
                    <!-- </div> -->
                </div>

        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>

    <!-- pagination -->
    <?php get_template_part('partials/page', 'links'); ?>

</div>

<?php get_footer(); ?>