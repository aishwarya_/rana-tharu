<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row">

                <div class="col-md-6">
                    <div class="heading brand-font">
                        <a href="category/bichar/">
                            <!-- <div class="d-flex justify-content-between"> -->
                            <h1 class="h4 text-center my-2 fw-bold text-white p-2" style="background-color: green;">विचार</h1>
                            <!-- <h1 class="h4 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                            <!-- </div> -->
                        </a>
                    </div>

                    <div class="row mx-1">

                        <!-- query -->

                        <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => '2',
                            'category_name' => 'bichar',
                            // 'order' => 'ASC'
                        );
                        $latest = new WP_Query($args);
                        if ($latest->have_posts()) {
                            while ($latest->have_posts()) : $latest->the_post();

                        ?>
                                <div class="col-md-12 mb-4">
                                    <div class="">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <?php if (has_post_thumbnail()) : ?>
                                                <img width="100%" style="aspect-ratio: 4/3; margin-bottom:5px;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                            <?php endif; ?>
                                        </a>
                                        <div class="mb-4 p-2 text-center">
                                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                                <h1 class=" h4 text-dark brand-font"><?php the_title(); ?></h1>
                                            </a>
                                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                                        </div>
                                    </div>
                                </div>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="heading brand-font">
                        <a href="category/interview/">
                            <!-- <div class="d-flex justify-content-between"> -->
                            <h1 class="h4 text-center my-2  fw-bold text-white p-2" style="background-color: green;">अन्तरवार्ता</h1>
                            <!-- <h1 class="h4 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                            <!-- </div> -->
                        </a>
                    </div>

                    <div class="row mx-1">

                        <!-- query -->

                        <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => '2',
                            'category_name' => 'interview',
                            // 'order' => 'ASC'
                        );
                        $latest = new WP_Query($args);
                        if ($latest->have_posts()) {
                            while ($latest->have_posts()) : $latest->the_post();

                        ?>

                                <div class="col-md-12 mb-4">
                                    <div class="">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <?php if (has_post_thumbnail()) : ?>
                                                <img width="100%" style="aspect-ratio: 4/3; margin-bottom:5px;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                            <?php endif; ?>
                                        </a>
                                        <div class="mb-4 p-2 text-center">
                                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                                <h1 class=" h4 text-dark brand-font"><?php the_title(); ?></h1>
                                            </a>
                                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                                        </div>
                                    </div>
                                </div>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="heading brand-font">
                <h1 class="h4 text-center my-2 fw-bold text-white p-2" style="background-color: green;">बिज्ञापन </h1>
            </div>

            <div class="container">
                <div class="ads-widget img-repo">
                    <?php dynamic_sidebar('sidebar'); ?>
                </div>
            </div>
        </div>
    </div>

</div>