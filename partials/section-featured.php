<div class="container">
    <div class="row my-3">
        <div class="d-flex justify-content-center mb-4 img-repo">
            <?php dynamic_sidebar('adstwo'); ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row my-4">

        <!-- query -->
        <?php
        $args = array(
            'post_type' => 'post',
            'category_name' => 'featured',
            'posts_per_page' => '2',
            // 'order' => 'ASC'
        );
        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-12 mb-4">
                    <div class=" text-center fw-bold ">
                        <a href="<?php the_permalink(); ?>">
                            <h1 class="news-txt brand-font"><?php the_title(); ?></h1>
                        </a>
                    </div>
                    <div class="time text-center text-dark my-3">
                        <i class="far fa-clock "></i> <span class="brand-font"><?php the_date(); ?></span>
                    </div>
                    <div class="mb-4 text-center">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <img width="100%" style="aspect-ratio: 8/4;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="txt brand-font text-dark 2h-lg" style="font-size: 28px;">
                        <?php echo custom_excerpt(); ?>
                    </div>
                </div>
                <hr>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="d-flex justify-content-center mb-4 img-repo">
            <?php dynamic_sidebar('adseight'); ?>
        </div>
    </div>
</div>