<div class="container">
    <div class="d-flex justify-content-center img-repo">
        <?php dynamic_sidebar('adsfour'); ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="heading brand-font">
                <a href="category/history/">
                    <!-- <div class="d-flex justify-content-between"> -->
                    <h1 class="h4 text-center my-2 fw-bold text-white p-2" style="background-color: green;">ईतिहास</h1>
                    <!-- <h1 class="h4 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                    <!-- </div> -->
                </a>
            </div>

            <div class="row mx-1">

                <!-- query -->

                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => '6',
                    'category_name' => 'history',
                    // 'order' => 'ASC'
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    while ($latest->have_posts()) : $latest->the_post();

                ?>
                        <div class="col-md-5 mb-4">
                            <div class="card text-center">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img width="100%" style="aspect-ratio: 4/3; margin-bottom:5px;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7 mb-4">
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h1 class=" h6 text-dark brand-font"><?php the_title(); ?></h1>
                            </a>
                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                        </div>


                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="heading brand-font">
                <a href="category/sports/">
                    <!-- <div class="d-flex justify-content-between"> -->
                    <h1 class="h4 text-center my-2 fw-bold text-white p-2" style="background-color: green;">खेलकुद</h1>
                    <!-- <h1 class="h4 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                    <!-- </div> -->
                </a>
            </div>

            <div class="row mx-1">

                <!-- query -->

                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => '2',
                    'category_name' => 'sports',
                    // 'order' => 'ASC'
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    while ($latest->have_posts()) : $latest->the_post();

                ?>
                        <div class="col-md-12 mb-4">
                            <div class="card text-center mb-2">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img width="100%" style="aspect-ratio: 4/3; margin-bottom:5px;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h1 class=" h6 text-dark brand-font"><?php the_title(); ?></h1>
                            </a>
                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                        </div>

                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="heading brand-font">
                <a href="category/agriculture/">
                    <!-- <div class="d-flex justify-content-between"> -->
                    <h1 class="h4 text-center my-2 fw-bold text-white p-2" style="background-color: green;">कृषि</h1>
                    <!-- <h1 class="h4 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                    <!-- </div> -->
                </a>
            </div>

            <div class="row mx-1">

                <!-- query -->

                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => '6',
                    'category_name' => 'agriculture',
                    // 'order' => 'ASC'
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    while ($latest->have_posts()) : $latest->the_post();

                ?>
                        <div class="col-md-5 mb-4">
                            <div class="card text-center">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img width="100%" style="aspect-ratio: 4/3; margin-bottom:5px;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7 mb-4 ">
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h1 class=" h6 text-dark brand-font"><?php the_title(); ?></h1>
                            </a>
                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                        </div>

                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
        </div>

    </div>

</div>

<div class="container">
    <div class="d-flex justify-content-center img-repo">
        <?php dynamic_sidebar('adsfive'); ?>
    </div>
</div>