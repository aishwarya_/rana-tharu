<div class="container">
    <div class="d-flex justify-content-center img-repo">
        <?php dynamic_sidebar('adsseven'); ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="heading brand-font">
            <a href="category/video/">
                <!-- <div class="d-flex justify-content-between"> -->
                <h1 class="h1 text-center my-2 fw-bold text-white p-2" style="background-color: blue;">भिडियो</h1>
                <!-- <h1 class="h2 text-end text-danger fw-bold mx-4">सबै >></h1> -->
                <!-- </div> -->
            </a>
        </div>

        <div class="row mt-3">

            <!-- query -->

            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => '4',
                'category_name' => 'video',
                // 'order' => 'ASC'
            );
            $latest = new WP_Query($args);
            if ($latest->have_posts()) {
                while ($latest->have_posts()) : $latest->the_post();

            ?>
                    <div class="col-md-6 mb-5">
                        <div class=" text-center mb-4">
                            <div class="video">
                                <?php the_field('video_url'); ?>
                            </div>
                        </div>
                        <div class="text-center">
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h1 class=" h4 fw-bold text-dark brand-font"><?php the_title(); ?></h1>
                            </a>
                            <i class="far fa-clock my-1"></i> <span class="brand-font"><?php the_date(); ?></span>
                        </div>
                    </div>

            <?php
                endwhile;
                wp_reset_postdata();
            }
            ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="d-flex justify-content-center img-repo">
        <?php dynamic_sidebar('adseleven'); ?>
    </div>
</div>