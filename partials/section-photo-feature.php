<div class="container">
    <div class="text-white" style="background-color: green;">
        <a href="category/photo-gallery/">
            <h1 class="h3 text-center p-3 text-brand fw-bold text-white">फोटो <span class="text-white">ग्यालरी</span></h1>
        </a>
    </div>
    <div class="row autoplay">
        <?php
        $wp_query = new WP_Query(array(
            'post_type' => 'post',
            'category__not_in' => [6],
            // 'posts_per_page' => 12,
            // 'offset' => '3',
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <div class="col-md-3">
                    <a href="category/photo-gallery/">
                        <div class="card my-3 p-2">
                            <img class="" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                            <!-- <h5 class="text-center text-muted fw-bold py-1"><?php the_title(); ?></h5> -->
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>