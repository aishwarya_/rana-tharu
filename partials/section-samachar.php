<div class="container">
    <div class="row">
        <div class="col-md-8">
            <a href="category/news/">
                <div class="heading brand-font" style="background-color: green;">
                    <h1 class="h1 text-center my-2 fw-bold text-white p-1">समाचार </h1>
                </div>
            </a>

            <div class="row brand-font">

                <!-- query -->
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => '1',
                    'category_name' => 'news',
                    // 'category__not_in' => [11],

                    // 'order' => 'ASC'
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    while ($latest->have_posts()) : $latest->the_post();

                ?>

                        <div class="text-center">
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h1 class="h1 text-dark brand-font p-1"><?php the_title(); ?></h1>
                            </a>
                            <i class="far fa-clock my-3"></i> <span class="brand-font"><?php the_date(); ?></span>
                        </div>



                        <div class="col-md-7 mt-3">
                            <a href="<?php echo get_the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="col-md-5 mt-3 text-dark" style="text-align: justify; font-size:18px">
                            <?php echo custom_news(); ?>
                        </div>

                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="heading brand-font" style="background-color: green;">
                <h1 class="h1 text-center my-2 fw-bold text-white p-1">बिज्ञापन </h1>
            </div>

            <div class="container">
                <div class="ads-widget img-repo">
                    <?php dynamic_sidebar('sidebar1'); ?>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="container">
    <div class="d-flex justify-content-center img-repo">
        <?php dynamic_sidebar('adsthree'); ?>
    </div>
</div>