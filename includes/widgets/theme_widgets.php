<?php
function init_theme_widgets(){
register_sidebar( array(
        'name' => 'मेनु भन्दा माथिको बिज्ञापन ',
        'id' => 'adsone',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement One'
    ) );
    register_sidebar( array(
        'name' => 'मेनु भन्दा तलको बिज्ञापन ',
        'id' => 'adstwo',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Two'
    ) );

    register_sidebar( array(
        'name' => ' समाचार भन्दा पछिको बिज्ञापन',
        'id' => 'adsthree',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Three'
    ) );
    register_sidebar( array(
        'name' => 'खेलकुद भन्दा माथिको बिज्ञापन',
        'id' => 'adsfour',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Four'
    ) );
    register_sidebar( array(
        'name' => 'खेलकुद भन्दा पछिको बिज्ञापन',
        'id' => 'adsfive',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Five'
    ) );
    register_sidebar( array(
        'name' => 'Category को first ma बिज्ञापन',
        'id' => 'adssix',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Six'
    ) );

    register_sidebar( array(
        'name' => 'भिडियो भन्दा माथिको बिज्ञापन',
        'id' => 'adsseven',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Eight'
    ) );

    register_sidebar( array(
        'name' => 'भिडियो भन्दा पछिको  बिज्ञापन',
        'id' => 'adseleven',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Eleven'
    ) );

    register_sidebar( array(
        'name' => 'featured समाचार भन्दा पछिको बिज्ञापन',
        'id' => 'adseight',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Ten'
    ) );

    register_sidebar( array(
        'name' => 'Single page को first ma बिज्ञापन',
        'id' => 'adsnine',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Single page ma menu vanda talako ads'
    ) );

    register_sidebar( array(
        'name' => 'सिंगल पेज को लास्टमा बिज्ञापन  ',
        'id' => 'adsten',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Advertisement Twelve'
    ) );

    register_sidebar( array(
        'name' => 'हाम्रो टिम',
        'id' => 'ourteam',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Hamro Team'
    ) );

    register_sidebar( array(
        'name' => 'सम्पर्क',
        'id' => 'contact',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'Contact'
    ) );

    register_sidebar( array(
        'name' => 'अन्तर्वार्ताको साइडमा बिज्ञापन ',
        'id' => 'sidebar',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'sidebar'
    ) );
    
    register_sidebar( array(
        'name' => 'समाचारको साइडमा बिज्ञापन ',
        'id' => 'sidebar1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="ads-title">',
        'after_title' => '</h3>',
        'description' => 'sidebar1'
    ) );

    
    // facebook page

    register_sidebar( array(
        'name' => 'हाम्रो बारेमा',
        'id' => 'about-us',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'description' => 'About Us'
    ) );
  
}
add_action('widgets_init', 'init_theme_widgets');